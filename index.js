var io = require('socket.io-client'),
    fs = require('fs'),
    colors = require('colors'),
    request = require('request'),
    moment = require('moment'),
    config = require('./config.json'),
    socket,
    myUserID,
    users,
    usersInRooms,
    rooms = {},
    inRooms = [],
    Discord = require('discord.io'),
    readline = require('readline'),
    bot = new Discord.Client({
        autorun: true,
        //email: config.email,
        //password: config.pass,
        token: config.DStoken
    })

function messageSend(meta) {
    bot.sendMessage({
        to: meta.channelID,
        message: meta.msg,
        typing: false
    })
}
// Look for key in the config file
if (!config.key) {
    throw new Error('Key not found, set it config.json from scrap.tf/devices');
}

// Look for client ID in the config file
if (!config.client_id) {
    throw new Error('Client ID not found, set it config.json');
}

// See if we already logged in the first time with the key
if (!config.token) {
    getToken();
} else {
    connect();
}

// This is used to change the short key from scrap.tf/devices into a token to connect to the socket
// We are responsible for saving the token this sends back
function getToken() {
    request.post('https://chat.scrap.tf/auth', {
        form: {
            key: config.key,
            id: config.client_id,
            client: config.client_name
        },
        json: true
    }, function(err, response, body) {

        if (body.success) {
            console.log('Login Success'.green);
            config.token = body.token;
            saveConfig();
        } else {
            console.log('Login Failed'.red);
            console.error(body.message);
        }

    });
}

// This is how we connect to the chat server
function connect() {
    socket = io('wss://chat.scrap.tf', {
        query: 'token=' + config.token + '&id=' + config.client_id + '&mobile=' + false,
        reconnection: true,
        timeout: 60000,
        transports: ['websocket']
    });
    bindSocketHandlers();
}

// This binds all the events the server sends back
function bindSocketHandlers() {

    // When we are connected to the chat server
    socket.on('connect', function(msg) {
        console.log('Connected to chat'.green);
    });

    socket.on('disconnect', function() {
        console.log('Disconnected'.red);
    });

    socket.on('disconnect reason', function(reason) {
        console.log('Disconnect Reason: '.red + reason);
    });

    // Once the user's profile is loaded and you can do things
    socket.on('user loaded', function() {
        console.log('User loaded, ready to join rooms'.green);
        joinRoom('home');
    });

    // Sent by the server to indicate who you are
    socket.on('user id', function(userid) {
        myUserID = userid;
        console.log(('User ID is ' + userid).green);
    });

    // This is data (username, avatar, color, etc) of every user sent whenever it's changed
    socket.on('users', function(data) {
        users = data;
    });
    // This is mostly when a user updates their profile or joins in more places
    socket.on('users update', function(data) {
        var old = users[data.id];
        if (old) {
            if (old.username != data.user.username || old.color != data.user.color || old.group != data.user.group) {
                // User has changed their username
            }
            if (old.avatar != data.user.avatar) {
                // User has changed their avatar
            }
        }
        users[data.id] = data.user;
    });

    // This is data on which user is in which rooms, this is sent separate from 'users' to reduce data usage
    socket.on('rooms', function(data) {
        usersInRooms = data;
    });

    // When we have joined a room
    socket.on('joined room', function(data) {
        console.log(('Joined Room: ' + data.room).green);
        var room = data.room;
        currentRoom = room;
        inRooms.push(room);
        rooms[room] = {
            display: data.display,
            message: data.message,
            nsfw: data.nsfw,
            panties: data.panties,
            owner: data.owner
        };
    });

    // Sent when a room changes
    socket.on('room update', function(data) {
        var room = data.room;
        rooms[room] = {
            display: data.display,
            message: data.message,
            nsfw: data.nsfw,
            panties: data.panties,
            owner: data.owner
        };
    });

    // When we left a room
    socket.on('left room', function(room) {
        delete inRooms[inRooms.indexOf(room)];
    });

    socket.on('user joined', function(userid) {
        console.log((users[userid].username + ' Joined Chat').yellow);
    });

    socket.on('user left', function(userid) {
        try {
            console.log((users[userid].username + ' Left Chat').yellow);
        } catch (e) { /**/ }
    });

    socket.on('user timeout', function(userid) {
        console.log((users[userid].username + ' Timed Out').yellow);
    });

    socket.on('user joined room', function(data) {
        try {
            console.log((users[data.userid].username + ' Joined "' + data.room + '"').yellow)
        } catch (e) { /**/ }
    })

    socket.on('user left room', function(data) {
        try {
            console.log((users[data.userid].username + ' Left "' + data.room + '"').yellow);
        } catch (e) { /**/ }
    });

    // When a message is received
    socket.on('message', function(data) {
        var user = users[data.userid],
            room = data.room,
            message = data.message,
            isAction = data.action,
            time = moment().format('LTS');

        if (isAction) {
            console.log('[' + time + ']'.grey + (' [' + room + '] ').yellow + user.username.cyan + ' ' + message);
        } else {
            console.log('[' + time + ']'.grey + (' [' + room + '] ').yellow + user.username.cyan + ': ' + message);
        }


        // This is where you react to user messages
        if (room == 'home') {
            if (isAction) {
                messageSend({
                    channelID: '224421801403482112',
                    msg: user.username + ": `" + message + "`"
                })
            } else {
                messageSend({
                    channelID: '224421801403482112',
                    msg: user.username + ": " + message
                })
            }
            if (message == '!bot') {
                //  sendMessage('Hello I am a bot!', room);
            } else if (isAction && message.match(/^hugs bot$/)) {
                //sendAction('hugs '+user.username, room);
            }

        }

    });

    socket.on('server', function(msg) {
        console.log('Server: '.red + msg);
    });

}

bot.on('message', function(user, userID, channelID, message, rawEvent) {
  var serverID = bot.channels[channelID].guild_id,
      cname = bot.servers[serverID].channels[channelID].name,
      sname = bot.servers[serverID].name
  meta = {
        userID: userID,
        user: user,
        channelID: channelID,
        serverID: serverID,
        serverName: sname,
        channelName: cname,
        messageID: rawEvent.d.id,
        rawEvent: rawEvent
    }
    if (cname === 'home' && userID === '70921043782402048') {
      sendMessage(message, 'home')
    }
})
// Other chat functions, etc

// Send normal messages
function sendMessage(message, room) {
    socket.emit('message', {
        message: message,
        room: room
    });
}

// This is to send /me messages
function sendAction(message, room) {
    socket.emit('action', {
        message: message,
        room: room
    });
}

// Join a room
function joinRoom(room) {
    socket.emit('join room', {
        room: room
    });
}

// Utils

function saveConfig() {
    fs.writeFile('config.json', JSON.stringify(config, null, 2), function(err) {
        if (err) {
            return console.log(err);
        }
        console.log('Saved config.json');
    });
}

function guidGenerator() {
    var S4 = function() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}


var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});
rl.on('line', function(line) {
    eval(line);
})